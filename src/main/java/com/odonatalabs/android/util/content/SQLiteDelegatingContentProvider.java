package com.odonatalabs.android.util.content;

import android.database.sqlite.SQLiteOpenHelper;

public abstract class SQLiteDelegatingContentProvider<T extends SQLiteOpenHelper> extends
		DelegatingContentProvider {
	protected T databaseHelper;
	
	public T getOpenHelperForTests() {
		return databaseHelper;
	}
}
