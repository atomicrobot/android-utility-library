package com.odonatalabs.android.util.content;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public abstract class ContentProviderDelegate {
	protected final Context context;
	protected final String authority;

	protected ContentProviderDelegate(Context context, String authority) {
		this.context = context;
		this.authority = authority;
	}

	protected String getAuthority() {
		return authority;
	}
	
	protected abstract String getUriPattern();

	protected ContentResolver getContentResolver() {
		return context.getContentResolver();
	}
	
	protected String getType(Uri uri) {
		throw new UnsupportedOperationException();
	}

	protected Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		throw new UnsupportedOperationException();
	}

	protected Uri insert(Uri uri, ContentValues values) {
		throw new UnsupportedOperationException();
	}

	protected int bulkInsert(Uri uri, ContentValues[] values) {
		throw new UnsupportedOperationException();
	}

	protected int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		throw new UnsupportedOperationException();
	}

	protected int delete(Uri uri, String selection, String[] selectionArgs) {
		throw new UnsupportedOperationException();
	}
}