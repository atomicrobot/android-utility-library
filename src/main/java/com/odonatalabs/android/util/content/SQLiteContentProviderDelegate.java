package com.odonatalabs.android.util.content;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class SQLiteContentProviderDelegate extends ContentProviderDelegate {
	protected SQLiteOpenHelper databaseHelper;
	
	protected SQLiteContentProviderDelegate(Context context, String authority, SQLiteOpenHelper databaseHelper) {
		super(context, authority);
		this.databaseHelper = databaseHelper;
	}
}