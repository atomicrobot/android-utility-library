package com.odonatalabs.android.util.content;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

public abstract class DelegatingContentProvider extends ContentProvider {

	private final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

	private ContentProviderDelegate[] delegates;

	@Override
	public boolean onCreate() {
		prepareProviders();
		setupDelegates();
		return true;
	}

	protected void prepareProviders() {
	}

	private void setupDelegates() {
		delegates = buildDelegates();
		
		for (int delegateIndex = 0; delegateIndex < delegates.length; delegateIndex++) {
			ContentProviderDelegate delegate = delegates[delegateIndex];
			String authority = delegate.getAuthority();
			String uriPattern = delegate.getUriPattern();
			uriMatcher.addURI(authority, uriPattern, delegateIndex);
		}
	}

	protected abstract String getAuthority();
	
	protected abstract ContentProviderDelegate[] buildDelegates();

	@Override
	public String getType(Uri uri) {
		ContentProviderDelegate delegate = getDelegate(uri);
		return delegate.getType(uri);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		ContentProviderDelegate delegate = getDelegate(uri);
		return delegate.query(uri, projection, selection, selectionArgs,
				sortOrder);
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		ContentProviderDelegate delegate = getDelegate(uri);
		return delegate.insert(uri, values);
	}
	
	@Override
	public int bulkInsert(Uri uri, ContentValues[] values) {
		ContentProviderDelegate delegate = getDelegate(uri);
		return delegate.bulkInsert(uri, values);
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		ContentProviderDelegate delegate = getDelegate(uri);
		return delegate.update(uri, values, selection, selectionArgs);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		ContentProviderDelegate delegate = getDelegate(uri);
		return delegate.delete(uri, selection, selectionArgs);
	}

	private ContentProviderDelegate getDelegate(Uri uri) {
		int delegateIndex = uriMatcher.match(uri);
		return delegates[delegateIndex];
	}
}
