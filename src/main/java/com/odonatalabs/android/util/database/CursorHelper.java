package com.odonatalabs.android.util.database;

import java.util.Date;
import java.util.UUID;

import android.database.Cursor;

public class CursorHelper {
	private static final int BOOLEAN_FALSE = 0;

	/**
	 * Will return true for any non-zero integer value that is returned by the
	 * cursor for this column and will return false only if the value returned
	 * by the cursor for this column is zero.
	 */
	public static boolean getBoolean(Cursor cursor, String columnName) {
		int rawBooleanValue = getInt(cursor, columnName);
		return (BOOLEAN_FALSE != rawBooleanValue);
	}

	/**
	 * Assumes that the stored value is the number of milliseconds since epoch
	 * as defined {@link Date}.
	 */
	public static Date getDate(Cursor cursor, String columnName) {
		long msSinceEpoch = getLong(cursor, columnName);
		return new Date(msSinceEpoch);
	}

	public static UUID getUUID(Cursor cursor, String columnName) {
		String rawUUID = getString(cursor, columnName);
		return UUID.fromString(rawUUID);
	}

	public static byte[] getBlob(Cursor cursor, String columnName) {
		int columnIndex = getColumnIndex(cursor, columnName);
		return cursor.getBlob(columnIndex);
	}

	public static String getString(Cursor cursor, String columnName) {
		int columnIndex = getColumnIndex(cursor, columnName);
		return cursor.getString(columnIndex);
	}

	public static short getShort(Cursor cursor, String columnName) {
		int columnIndex = getColumnIndex(cursor, columnName);
		return cursor.getShort(columnIndex);
	}

	public static int getInt(Cursor cursor, String columnName) {
		int columnIndex = getColumnIndex(cursor, columnName);
		return cursor.getInt(columnIndex);
	}

	public static long getLong(Cursor cursor, String columnName) {
		int columnIndex = getColumnIndex(cursor, columnName);
		return cursor.getLong(columnIndex);
	}

	public static float getFloat(Cursor cursor, String columnName) {
		int columnIndex = getColumnIndex(cursor, columnName);
		return cursor.getFloat(columnIndex);
	}

	public static double getDouble(Cursor cursor, String columnName) {
		int columnIndex = getColumnIndex(cursor, columnName);
		return cursor.getDouble(columnIndex);
	}

	public static boolean isNull(Cursor cursor, String columnName) {
		int columnIndex = getColumnIndex(cursor, columnName);
		return cursor.isNull(columnIndex);
	}

	private static int getColumnIndex(Cursor cursor, String columnName) {
		return cursor.getColumnIndex(columnName);
	}

	private CursorHelper() {
	}
}
