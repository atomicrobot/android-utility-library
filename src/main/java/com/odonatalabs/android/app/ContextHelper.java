package com.odonatalabs.android.app;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;

public class ContextHelper {
	public static NotificationManager getNotificationManager(Context context) {
		return (NotificationManager) context
				.getSystemService(Activity.NOTIFICATION_SERVICE);
	}
}
