package com.odonatalabs.android.util.database;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;

public class CursorHelperTest {
	@Test
	public void testGetBlob() {
		Cursor cursor = MockCursor.buildBlobCursor("Yay");
		byte[] actual = CursorHelper.getBlob(cursor, MockCursor.COLUMN_NAME);
		Assert.assertArrayEquals("Yay".getBytes(), actual);
	}
	
	@Test
	public void testGetBlobFailing() {
		Cursor cursor = MockCursor.buildBlobCursor("Yay");
		try {
			CursorHelper.getBlob(cursor, MockCursor.BAD_COLUMN_NAME);
			Assert.fail();
		} catch (IllegalArgumentException ex) {
			// Woo hoo!
		}
	}

	@Test
	public void testGetString() {
		Cursor cursor = MockCursor.buildStringCursor("Yay");
		String actual = CursorHelper.getString(cursor, MockCursor.COLUMN_NAME);
		Assert.assertEquals("Yay", actual);
	}

	@Test
	public void testGetStringFailing() {
		Cursor cursor = MockCursor.buildStringCursor("Yay");
		try {
			CursorHelper.getString(cursor, MockCursor.BAD_COLUMN_NAME);
			Assert.fail();
		} catch (IllegalArgumentException ex) {
			// Woo hoo!
		}
	}

	@Test
	public void testGetShort() {
		Cursor cursor = MockCursor.buildShortCursor((short) 123);
		short actual = CursorHelper.getShort(cursor, MockCursor.COLUMN_NAME);
		Assert.assertEquals(123, actual);
	}

	@Test
	public void testGetShortFailing() {
		Cursor cursor = MockCursor.buildShortCursor((short) 123);
		try {
			CursorHelper.getShort(cursor, MockCursor.BAD_COLUMN_NAME);
			Assert.fail();
		} catch (IllegalArgumentException ex) {
			// Woo hoo!
		}
	}
	
	@Test
	public void testGetInt() {
		Cursor cursor = MockCursor.buildIntCursor(12345);
		int actual = CursorHelper.getInt(cursor, MockCursor.COLUMN_NAME);
		Assert.assertEquals(12345, actual);
	}

	@Test
	public void testGetIntFailing() {
		Cursor cursor = MockCursor.buildIntCursor(12345);
		try {
			CursorHelper.getInt(cursor, MockCursor.BAD_COLUMN_NAME);
			Assert.fail();
		} catch (IllegalArgumentException ex) {
			// Woo hoo!
		}
	}
	
	@Test
	public void testGetLong() {
		Cursor cursor = MockCursor.buildLongCursor(12345);
		long actual = CursorHelper.getLong(cursor, MockCursor.COLUMN_NAME);
		Assert.assertEquals(12345, actual);
	}

	@Test
	public void testGetLongFailing() {
		Cursor cursor = MockCursor.buildLongCursor(12345);
		try {
			CursorHelper.getLong(cursor, MockCursor.BAD_COLUMN_NAME);
			Assert.fail();
		} catch (IllegalArgumentException ex) {
			// Woo hoo!
		}
	}
	
	@Test
	public void testGetFloat() {
		Cursor cursor = MockCursor.buildFloatCursor(1.234f);
		float actual = CursorHelper.getFloat(cursor, MockCursor.COLUMN_NAME);
		Assert.assertEquals(1.234f, actual, 0);
	}

	@Test
	public void testGetFloatFailing() {
		Cursor cursor = MockCursor.buildFloatCursor(1.234f);
		try {
			CursorHelper.getFloat(cursor, MockCursor.BAD_COLUMN_NAME);
			Assert.fail();
		} catch (IllegalArgumentException ex) {
			// Woo hoo!
		}
	}

	@Test
	public void testGetDouble() {
		Cursor cursor = MockCursor.buildDoubleCursor(1.2345);
		double actual = CursorHelper.getDouble(cursor, MockCursor.COLUMN_NAME);
		Assert.assertEquals(1.2345, actual, 0);
	}

	@Test
	public void testGetDoubleFailing() {
		Cursor cursor = MockCursor.buildDoubleCursor(1.2345);
		try {
			CursorHelper.getDouble(cursor, MockCursor.BAD_COLUMN_NAME);
			Assert.fail();
		} catch (IllegalArgumentException ex) {
			// Woo hoo!
		}
	}
	
	@Test
	public void testIsNull() {
		Cursor cursor = MockCursor.buildIsNullCursor(true);
		boolean actual = CursorHelper.isNull(cursor, MockCursor.COLUMN_NAME);
		Assert.assertEquals(true, actual);
	}

	@Test
	public void testIsNullFailing() {
		Cursor cursor = MockCursor.buildIsNullCursor(true);
		try {
			CursorHelper.isNull(cursor, MockCursor.BAD_COLUMN_NAME);
			Assert.fail();
		} catch (IllegalArgumentException ex) {
			// Woo hoo!
		}
	}

	@Test
	public void testGetBooleanReturnsFalse() {
		Cursor cursor = MockCursor.buildIntCursor(0);
		boolean actual = CursorHelper.getBoolean(cursor, MockCursor.COLUMN_NAME);
		Assert.assertFalse(actual);
	}
	
	@Test
	public void testGetBooleanReturnsTrue() {
		Cursor cursor = MockCursor.buildIntCursor(42);
		boolean actual = CursorHelper.getBoolean(cursor, MockCursor.COLUMN_NAME);
		Assert.assertTrue(actual);
	}
	
	@Test
	public void testGetDate() {
		Date now = new Date();
		
		Cursor cursor = MockCursor.buildLongCursor(now.getTime());
		Date actual = CursorHelper.getDate(cursor, MockCursor.COLUMN_NAME);
		Assert.assertEquals(now, actual);
	}
	
	private static class MockCursor implements Cursor {
		static final String BAD_COLUMN_NAME = "not gonna work";
		static final String COLUMN_NAME = "does not matter";
		static final int COLUMN_INDEX = 42;

		static Cursor buildBlobCursor(final String valueBytes) {
			return new MockCursor() {
				@Override
				public byte[] getBlob(int columnIndex) {
					assertColumnIndexIsValid(columnIndex);
					return valueBytes.getBytes();
				}
			};
		}

		static Cursor buildStringCursor(final String value) {
			return new MockCursor() {
				@Override
				public String getString(int columnIndex) {
					assertColumnIndexIsValid(columnIndex);
					return value;
				}
			};
		}

		static Cursor buildShortCursor(final short value) {
			return new MockCursor() {
				@Override
				public short getShort(int columnIndex) {
					assertColumnIndexIsValid(columnIndex);
					return value;
				}
			};
		}
		
		static Cursor buildIntCursor(final int value) {
			return new MockCursor() {
				@Override
				public int getInt(int columnIndex) {
					assertColumnIndexIsValid(columnIndex);
					return value;
				}
			};
		}
		
		static Cursor buildLongCursor(final long value) {
			return new MockCursor() {
				@Override
				public long getLong(int columnIndex) {
					assertColumnIndexIsValid(columnIndex);
					return value;
				}
			};
		}
		
		static Cursor buildFloatCursor(final float value) {
			return new MockCursor() {
				@Override
				public float getFloat(int columnIndex) {
					assertColumnIndexIsValid(columnIndex);
					return value;
				}
			};
		}
		
		static Cursor buildDoubleCursor(final double value) {
			return new MockCursor() {
				@Override
				public double getDouble(int columnIndex) {
					assertColumnIndexIsValid(columnIndex);
					return value;
				}
			};
		}
		
		static Cursor buildIsNullCursor(final boolean value) {
			return new MockCursor() {
				@Override
				public boolean isNull(int columnIndex) {
					assertColumnIndexIsValid(columnIndex);
					return value;
				}
			};
		}
		
		private MockCursor() {
		}

		protected void assertColumnIndexIsValid(int columnIndex) {
			if (COLUMN_INDEX != columnIndex) {
				throw new IllegalArgumentException();
			}
		}

		@Override
		public int getColumnIndex(String columnName) {
			if (COLUMN_NAME.equals(columnName)) {
				return COLUMN_INDEX;
			}

			return -1;
		}

		@Override
		public int getCount() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getPosition() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean move(int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean moveToPosition(int position) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean moveToFirst() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean moveToLast() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean moveToNext() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean moveToPrevious() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isFirst() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isLast() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isBeforeFirst() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isAfterLast() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getColumnIndexOrThrow(String columnName)
				throws IllegalArgumentException {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getColumnName(int columnIndex) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String[] getColumnNames() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getColumnCount() {
			throw new UnsupportedOperationException();
		}

		@Override
		public byte[] getBlob(int columnIndex) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getString(int columnIndex) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public short getShort(int columnIndex) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getInt(int columnIndex) {
			throw new UnsupportedOperationException();
		}

		@Override
		public long getLong(int columnIndex) {
			throw new UnsupportedOperationException();
		}

		@Override
		public float getFloat(int columnIndex) {
			throw new UnsupportedOperationException();
		}

		@Override
		public double getDouble(int columnIndex) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isNull(int columnIndex) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void deactivate() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean requery() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void close() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isClosed() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void registerContentObserver(ContentObserver observer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void unregisterContentObserver(ContentObserver observer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void registerDataSetObserver(DataSetObserver observer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void unregisterDataSetObserver(DataSetObserver observer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void setNotificationUri(ContentResolver cr, Uri uri) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean getWantsAllOnMoveCalls() {
			throw new UnsupportedOperationException();
		}

		@Override
		public Bundle getExtras() {
			throw new UnsupportedOperationException();
		}

		@Override
		public Bundle respond(Bundle extras) {
			throw new UnsupportedOperationException();
		}
	}
}
